package Controllers;

import DAOs.HorarioDaViagemDAO;
import Models.HorarioDaViagemModel;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ServletCadastrarHorarioDaViagem", urlPatterns = {"/cadastrarHorarioDaViagem"})
public class ServletCadastrarHorarioDaViagem extends HttpServlet {

    private HttpSession sessao;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.sessao = request.getSession();
        
        //System.out.println("asdasd");

        RequestDispatcher rd = null; //setando o objeto "despachador"
        //Colocar o mesmo parametro que esta no type do html
        String viagemId = request.getParameter("viagemId");
        String horarioId = request.getParameter("horarioId");

        HorarioDaViagemModel viagem = new HorarioDaViagemModel(Integer.parseInt(viagemId), Integer.parseInt(horarioId));
        HorarioDaViagemDAO dao = new HorarioDaViagemDAO();

        try {
            dao.salvar(viagem);//feito
        } catch (Exception ex) {

        }
        response.sendRedirect("./Views/Adm/horarioDaViagemView.jsp");
        //rd = request.getRequestDispatcher("/Views/Adm/viagemView.jsp");
//        despachando para a página setada, segundo as opções acima
        //rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
