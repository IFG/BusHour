package Controllers;

import DAOs.RodoviariaDAO;
import Models.RodoviariaModel;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ServletCadastrarRodoviaria", urlPatterns = {"/cadastrarRodoviaria"})
public class ServletCadastrarRodoviaria extends HttpServlet {

    private HttpSession sessao;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.sessao = request.getSession();

        RequestDispatcher rd = null; //setando o objeto "despachador"
        //Colocar o mesmo parametro que esta no type do html
        String rodoviariaNome = request.getParameter("rodoviariaNome");
        String rodoviariaEndereco = request.getParameter("rodoviariaEndereco");
        String rodoviariaTelefone = request.getParameter("rodoviariaTelefone");
        String rodoviariaHorarioAtendimento = request.getParameter("rodoviariaHorarioAtendimento");
        String cidadeId = request.getParameter("cidadeId");

        RodoviariaModel rodoviaria = new RodoviariaModel(0, rodoviariaNome, rodoviariaEndereco, rodoviariaTelefone, rodoviariaHorarioAtendimento, Integer.parseInt(cidadeId));
        RodoviariaDAO dao = new RodoviariaDAO();

        try {
            dao.salvar(rodoviaria);//feito
        } catch (Exception ex) {

        }
        response.sendRedirect("./Views/Adm/rodoviariaView.jsp");
        //rd = request.getRequestDispatcher("/Views/Adm/rodoviariaView.jsp");
//        despachando para a página setada, segundo as opções acima
        //rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
