package Controllers;

import DAOs.ViacaoDAO;
import Models.ViacaoModel;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ServletCadastrarViacao", urlPatterns = {"/cadastrarViacao"})
public class ServletCadastrarViacao extends HttpServlet {

    private HttpSession sessao;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.sessao = request.getSession();

        RequestDispatcher rd = null; //setando o objeto "despachador"
        //Colocar o mesmo parametro que esta no type do html
        String viacaoNome = request.getParameter("viacaoNome");
        String viacaoHistorico = request.getParameter("viacaoHistorico");
        String viacaoTelefone = request.getParameter("viacaoTelefone");
        
        ViacaoModel viacao = new ViacaoModel(0, viacaoNome, viacaoHistorico, viacaoTelefone);
        ViacaoDAO dao = new ViacaoDAO();

        try {
            dao.salvar(viacao);//feito
        } catch (Exception ex) {

        }
        response.sendRedirect("./Views/Adm/viacaoView.jsp");
        //rd = request.getRequestDispatcher("/Views/Adm/viacaoView.jsp");
//        despachando para a página setada, segundo as opções acima
        //rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
