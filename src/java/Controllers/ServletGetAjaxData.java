package Controllers;

import DAOs.CidadeDAO;
import DAOs.EstadoDAO;
import DAOs.HorarioDAO;
import DAOs.RodoviariaDAO;
import DAOs.ViacaoDAO;
import DAOs.ViagemDAO;
import Models.CidadeModel;
import Models.EstadoModel;
import Models.HorarioModel;
import Models.RodoviariaModel;
import Models.ViacaoModel;
import Models.ViagemModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ServletGetAjaxData", urlPatterns = {"/getDados"})
public class ServletGetAjaxData extends HttpServlet {

    HttpServletRequest request;
    HttpServletResponse response;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;

        String cmd = (request.getParameter("cmd") != null) ? request.getParameter("cmd").toLowerCase().toString() : "";
        ArrayList listaRetorno = new ArrayList();
        //RequestDispatcher rd =  request.getRequestDispatcher("/home.jsp");

        switch (cmd) {
//            case "listaviacoes_":
//                setViewData(getListaViacoes(), "lista-viacoes", "viacoes");
//                abreViewDestino("./Data/listaJSON.jsp");
//                break;
            case "listaestados":
                setViewData(getListaEstados(), "lista-estados", "estados");
                abreViewDestino("./Data/listaJSON.jsp");
                break;
            case "listacidades":
                int idEstado = Integer.parseInt(request.getParameter("idEstado"));
                setViewData(getListaFiltraCidades(idEstado), "lista-cidades", "cidades");
                abreViewDestino("./Data/listaJSON.jsp");
                break;
            case "listacidades2":
                setViewData(getListaFiltraCidades2(), "lista-cidades", "cidades");
                abreViewDestino("./Data/listaJSON.jsp");
                break;
            case "listarodoviarias":
                int idCidade = Integer.parseInt(request.getParameter("idCidade"));
                setViewData(getListaFiltraRodoviarias(idCidade), "lista-rodoviarias", "rodoviarias");
                abreViewDestino("./Data/listaJSON.jsp");
                break;
            case "listarodoviarias2":
                setViewData(getListaFiltraRodoviarias2(), "lista-rodoviarias", "rodoviarias");
                abreViewDestino("./Data/listaJSON.jsp");
                break;
            case "listaviacoes":
                int idRodoviaria = Integer.parseInt(request.getParameter("idRodoviaria"));
                setViewData(getListaFiltraViacoes(idRodoviaria), "lista-viacoes", "viacoes");
                abreViewDestino("./Data/listaJSON.jsp");
                break;
            case "listaviacoes2":
                setViewData(getListaFiltraViacoes2(), "lista-viacoes", "viacoes");
                abreViewDestino("./Data/listaJSON.jsp");
                break;
            case "listacidadedestino":
                int idCidadeOrigem = Integer.parseInt(request.getParameter("idCidadeOrigem"));
                int idRodoviaria2 = Integer.parseInt(request.getParameter("idRodoviaria"));
                int idViacao = Integer.parseInt(request.getParameter("idViacao"));
                setViewData(getListaFiltraCidades(idCidadeOrigem, idRodoviaria2, idViacao), "lista-cidadedestino", "cidadedestino");
                abreViewDestino("./Data/listaJSON.jsp");
                break;
            case "listahorario":
                int idCidadeOrigem2 = Integer.parseInt(request.getParameter("idCidadeOrigem"));
                int idCidadeDestino2 = Integer.parseInt(request.getParameter("idCidadeDestino"));
                int idRodoviaria3 = Integer.parseInt(request.getParameter("idRodoviaria"));
                int idViacao2 = Integer.parseInt(request.getParameter("idViacao"));
                setViewData(getListaHorarios(idCidadeOrigem2, idCidadeDestino2, idRodoviaria3, idViacao2), "lista-horario", "horarios");
                abreViewDestino("./Data/listaJSON.jsp");
                break;
            case "listahorario2":
                setViewData(getListaHorarios2(), "lista-horarios", "horarios2");
                abreViewDestino("./Data/listaJSON.jsp");
                break;
            case "listaviagem":
                setViewData(getListaViagem(), "lista-viagem", "viagens");
                abreViewDestino("./Data/listaJSON.jsp");
                break;
            case "listatodosdados":
                int idCidadeOrigem3 = Integer.parseInt(request.getParameter("cidadeOrigemId"));
                int idRodoviaria4 = Integer.parseInt(request.getParameter("rodoviariaOrigemId"));
                int idViacao3 = Integer.parseInt(request.getParameter("viacaoOrigemId"));
                int idCidadeDestino = Integer.parseInt(request.getParameter("cidadeDestinoId"));
                setViewData(getListaHorarios(idCidadeOrigem3, idCidadeDestino, idRodoviaria4, idViacao3), "lista-dados-tabela", "dadosList");
                abreViewDestino("./Views/horarioView.jsp");
                break;
            default:
                abreViewDestino("");
        }
    }

//    private ArrayList getListaViacoes() {
//        ViacaoDAO dao = new ViacaoDAO();
//        try {
//            ArrayList<ViacaoModel> lista = (ArrayList<ViacaoModel>) dao.listaTodos();
//            //Se a lista tiver dados retorna
//            return lista;
//        } catch (Exception ex) {
//            Logger.getLogger(ServletGetAjaxData.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        //Senao retorna nula
//        return null;
//    }
    private ArrayList getListaEstados() {
        EstadoDAO dao = new EstadoDAO();
        try {
            ArrayList<EstadoModel> lista = (ArrayList<EstadoModel>) dao.listaTodos();
            //Se a lista tiver dados retorna
            return lista;
        } catch (Exception ex) {
            Logger.getLogger(ServletGetAjaxData.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Senao retorna nula
        return null;
    }

    private ArrayList getListaFiltraCidades(int idEstado) {
        CidadeDAO dao = new CidadeDAO();
        try {
            ArrayList<CidadeModel> lista = (ArrayList<CidadeModel>) dao.listaTodasCidadesPorEstado(idEstado);
            //System.out.println(lista.get(1).getCidadeNome());
            return lista;
        } catch (Exception ex) {
            Logger.getLogger(ServletGetAjaxData.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Senao retorna nula
        return null;
    }

    private ArrayList getListaFiltraCidades2() {
        CidadeDAO dao = new CidadeDAO();
        try {
            ArrayList<CidadeModel> lista = (ArrayList<CidadeModel>) dao.listaTodos();
            return lista;
        } catch (Exception ex) {
            Logger.getLogger(ServletGetAjaxData.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Senao retorna nula
        return null;
    }

    private ArrayList getListaFiltraRodoviarias(int idCidade) {
        RodoviariaDAO dao = new RodoviariaDAO();
        try {
            ArrayList<RodoviariaModel> lista = (ArrayList<RodoviariaModel>) dao.listaTodasRodoviariasPorCidade(idCidade);
            return lista;
        } catch (Exception ex) {
            Logger.getLogger(ServletGetAjaxData.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Senao retorna nula
        return null;
    }

    private ArrayList getListaFiltraRodoviarias2() {
        RodoviariaDAO dao = new RodoviariaDAO();
        try {
            ArrayList<RodoviariaModel> lista = (ArrayList<RodoviariaModel>) dao.listaTodos();
            return lista;
        } catch (Exception ex) {
            Logger.getLogger(ServletGetAjaxData.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Senao retorna nula
        return null;
    }

    private ArrayList getListaFiltraViacoes(int idRodoviaria) {
        ViacaoDAO dao = new ViacaoDAO();
        try {
            ArrayList<ViacaoModel> lista = (ArrayList<ViacaoModel>) dao.listaTodosPorRodoviaria(idRodoviaria);
            return lista;
        } catch (Exception ex) {
            Logger.getLogger(ServletGetAjaxData.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Senao retorna nula
        return null;
    }

    private ArrayList getListaFiltraViacoes2() {
        ViacaoDAO dao = new ViacaoDAO();
        try {
            ArrayList<ViacaoModel> lista = (ArrayList<ViacaoModel>) dao.listaTodos();
            return lista;
        } catch (Exception ex) {
            Logger.getLogger(ServletGetAjaxData.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Senao retorna nula
        return null;
    }

    private ArrayList getListaFiltraCidades(int idCidadeOrigem, int idRodoviaria, int idViacao) {
        CidadeDAO dao = new CidadeDAO();
        try {
            ArrayList<CidadeModel> lista = (ArrayList<CidadeModel>) dao.listaCidadeDaViagem(idCidadeOrigem, idRodoviaria, idViacao);
            return lista;
        } catch (Exception ex) {
            Logger.getLogger(ServletGetAjaxData.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Senao retorna nula
        return null;
    }

    private ArrayList getListaHorarios(int idCidadeOrigem, int idCidadeDestino, int idRodoviaria, int idViacao) {
        HorarioDAO dao = new HorarioDAO();
        try {
            ArrayList<HorarioModel> lista = (ArrayList<HorarioModel>) dao.listaTodosHorariosDaViagem(idCidadeOrigem, idCidadeDestino, idRodoviaria, idViacao);
            //Se a lista tiver dados retorna
            return lista;
        } catch (Exception ex) {
            Logger.getLogger(ServletGetAjaxData.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Senao retorna nula
        return null;
    }

    private ArrayList getListaHorarios2() {
        HorarioDAO dao = new HorarioDAO();
        try {
            ArrayList<HorarioModel> lista = (ArrayList<HorarioModel>) dao.listaTodos();
            //Se a lista tiver dados retorna
            return lista;
        } catch (Exception ex) {
            Logger.getLogger(ServletGetAjaxData.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Senao retorna nula
        return null;
    }

    private ArrayList getListaViagem() {
        ViagemDAO dao = new ViagemDAO();
        try {
            ArrayList<ArrayList> lista = (ArrayList<ArrayList>) dao.listaTodos();
            //Se a lista tiver dados retorna
            return lista;
        } catch (Exception ex) {
            Logger.getLogger(ServletGetAjaxData.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Senao retorna nula
        return null;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void setViewData(ArrayList lista, String nmParametro, String classe) {
        this.request.setAttribute(nmParametro, lista);
        this.request.setAttribute("classe", classe);
    }

    private void abreViewDestino(String urlDestino) throws ServletException, IOException {
        if (urlDestino != "") {
            //System.out.println("abriu");
            this.request.getRequestDispatcher(urlDestino).forward(this.request, this.response);
        } else {
            this.response.sendRedirect("./");
        }
    }

}
