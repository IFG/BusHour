package DAOs;

import Models.CidadeModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CidadeDAO implements DAO {

    @Override
    public void atualizar(Object ob) throws Exception {
        CidadeModel vModel = (CidadeModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("UPDATE cidades SET cidade_nome = ?, cidade_ativo=?, estado_id=? WHERE cidade_id=?");
            ps.setString(1, vModel.getCidadeNome());
            ps.setString(2, vModel.getCidadeAtivo());
            ps.setInt(3, vModel.getEstadoId());
            ps.setInt(4, vModel.getCidadeId());

            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao atualizar dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    @Override
    public void excluir(Object ob) throws Exception {
        CidadeModel vModel = (CidadeModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("DELETE FROM cidades WHERE cidade_id=?");

            ps.setInt(1, vModel.getEstadoId());
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao excluir dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    @Override
    public List listaTodos() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<CidadeModel> list = new ArrayList<>();

        try {
            String SQL = "SELECT * FROM cidades";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);
            rs = ps.executeQuery();

            while (rs.next()) {
                CidadeModel cidades = new CidadeModel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
                list.add(cidades);
            }
        } catch (SQLException sqle) {
            throw new Exception("Erro ao buscar todos: \n" + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return list;
    }

    public List listaTodasCidadesPorEstado(int estadoId) throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<CidadeModel> list = new ArrayList<>();

        try {
            String SQL = "SELECT * FROM cidades WHERE estado_id=?";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);
            ps.setInt(1, estadoId);
            rs = ps.executeQuery();
            while (rs.next()) {
                CidadeModel cidades = new CidadeModel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
                list.add(cidades);
            }
        } catch (SQLException sqle) {
            throw new Exception("Erro ao buscar todos: \n" + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return list;
    }

    @Override
    public List procura(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void salvar(Object ob) throws Exception {
        CidadeModel vModel = (CidadeModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            String SQL = "INSERT INTO cidades (cidade_id, cidade_nome, cidade_ativo, estado_id) value (null, ?, ?, ?)";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);

            ps.setString(1, vModel.getCidadeNome());
            ps.setString(2, vModel.getCidadeAtivo());
            ps.setInt(3, vModel.getEstadoId());

            ps.executeUpdate();
        } catch (SQLException sqle) {
            System.out.println(sqle);
            throw new Exception("Erro ao inserir dados: \n" + sqle);

        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    public List listaCidadeDaViagem(int cidadeOrigemId, int rodoviariaId, int viacaoId) throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<CidadeModel> list = new ArrayList<>();

        try {
            String SQL = "SELECT * FROM cidades WHERE cidade_id In (SELECT cidade_destino_id FROM viagens WHERE cidade_origem_id=? and rodoviaria_id=? and viacao_id=?)";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);

            ps.setInt(1, cidadeOrigemId);
            ps.setInt(2, rodoviariaId);
            ps.setInt(3, viacaoId);

            rs = ps.executeQuery();
            while (rs.next()) {
                CidadeModel cidades = new CidadeModel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4));
                list.add(cidades);
            }
        } catch (SQLException sqle) {
            throw new Exception("Erro ao buscar todos: \n" + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        System.out.println(list);
        return list;
    }
}
