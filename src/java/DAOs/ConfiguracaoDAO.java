package DAOs;

import Models.ConfiguracaoModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConfiguracaoDAO implements DAO {

    @Override
    public void atualizar(Object ob) throws Exception {
        ConfiguracaoModel vModel = (ConfiguracaoModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("UPDATE configuracoes SET estado_id = ?, cidade_id=?, rodoviaria_id=? WHERE configuracao_id=?");
            ps.setInt(1, vModel.getEstadoId());
            ps.setInt(2, vModel.getCidadeId());
            ps.setInt(3, vModel.getRodoviariaId());
            ps.setInt(4, vModel.getConfiguracoesId());

            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao atualizar dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    @Override
    public void excluir(Object ob) throws Exception {
        ConfiguracaoModel vModel = (ConfiguracaoModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("DELETE FROM configuracoes WHERE configuracao_id=?");

            ps.setInt(1, vModel.getConfiguracoesId());
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao excluir dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    @Override
    public List listaTodos() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<ConfiguracaoModel> list = new ArrayList<>();

        try {
            String SQL = "SELECT * FROM cidades";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);
            rs = ps.executeQuery();

            while (rs.next()) {
                ConfiguracaoModel cidades = new ConfiguracaoModel(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4));
                list.add(cidades);
            }
        } catch (SQLException sqle) {
            throw new Exception("Erro ao buscar todos: \n" + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return list;
    }

    @Override
    public List procura(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void salvar(Object ob) throws Exception {
        ConfiguracaoModel vModel = (ConfiguracaoModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            String SQL = "INSERT INTO configuracoes (configuracao_id, estado_id, cidade_id, rodoviaria_id) value (null, ?, ?, ?)";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);

            ps.setInt(1, vModel.getEstadoId());
            ps.setInt(2, vModel.getCidadeId());
            ps.setInt(3, vModel.getRodoviariaId());

            ps.executeUpdate();
        } catch (SQLException sqle) {
            System.out.println(sqle);
            throw new Exception("Erro ao inserir dados: \n" + sqle);

        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }
}
