package DAOs;

import Models.EstadoModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EstadoDAO implements DAO {

    @Override
    public void atualizar(Object ob) throws Exception {
        EstadoModel vModel = (EstadoModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("UPDATE estados SET estado_nome = ?, estado_ativo=? WHERE estado_id=?");
            ps.setString(1, vModel.getEstadoNome());
            ps.setString(2, vModel.getEstadoAtivo());
            ps.setInt(3, vModel.getEstadoId());

            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao atualizar dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    @Override
    public void excluir(Object ob) throws Exception {
        EstadoModel vModel = (EstadoModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("DELETE FROM estados WHERE estados_id=?");

            ps.setInt(1, vModel.getEstadoId());
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao excluir dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    @Override
    public List listaTodos() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<EstadoModel> list = new ArrayList<>();

        try {
            String SQL = "SELECT * FROM estados";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);
            rs = ps.executeQuery();

            while (rs.next()) {
                EstadoModel estados = new EstadoModel(rs.getInt(1), rs.getString(2), rs.getString(3));
                list.add(estados);
            }
        } catch (SQLException sqle) {
            throw new Exception("Erro ao buscar todos: \n" + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }

        return list;
    }

    @Override
    public List procura(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void salvar(Object ob) throws Exception {
        EstadoModel vModel = (EstadoModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            String SQL = "INSERT INTO estados (estado_id, estado_nome, estado_ativo) value (null, ?, ?)";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);

            ps.setString(1, vModel.getEstadoNome());
            ps.setString(2, vModel.getEstadoAtivo());

            ps.executeUpdate();
        } catch (SQLException sqle) {
            System.out.println(sqle);
            throw new Exception("Erro ao inserir dados: \n" + sqle);

        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }
}
