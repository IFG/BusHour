package DAOs;

import Models.HorarioModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HorarioDAO implements DAO {

    @Override
    public void atualizar(Object ob) throws Exception {
        HorarioModel vModel = (HorarioModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("UPDATE horarios SET horario_saida = ?, horario_chegada=? WHERE horario_id=? and horario_id=?");
            ps.setTime(1, vModel.getHorarioSaida());
            ps.setTime(2, vModel.getHorarioChegada());
            ps.setInt(3, vModel.getHorarioId());

            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao atualizar dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    @Override
    public void excluir(Object ob) throws Exception {
        HorarioModel vModel = (HorarioModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("DELETE FROM horarios WHERE horario_id=?");

            ps.setInt(1, vModel.getHorarioId());

            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao excluir dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    @Override
    public List listaTodos() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<HorarioModel> list = new ArrayList<>();

        try {
            String SQL = "SELECT * FROM horarios";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);
            rs = ps.executeQuery();

            while (rs.next()) {
                HorarioModel horarios = new HorarioModel(rs.getInt(1), rs.getTime(2), rs.getTime(3));
                list.add(horarios);
            }
        } catch (SQLException sqle) {
            throw new Exception("Erro ao buscar todos: \n" + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return list;
    }

    @Override
    public List procura(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void salvar(Object ob) throws Exception {
        HorarioModel vModel = (HorarioModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            String SQL = "INSERT INTO horarios (horario_id, horario_saida, horario_chegada) value (null, ?, ?)";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);

            ps.setTime(1, vModel.getHorarioSaida());
            ps.setTime(2, vModel.getHorarioChegada());
            
            System.out.println(ps);
            
            ps.executeUpdate();
        } catch (SQLException sqle) {
            System.out.println(sqle);
            throw new Exception("Erro ao inserir dados: \n" + sqle);

        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    public List listaTodosHorariosDaViagem(int cidadeOrigemId, int cidadeDestinoId, int rodoviariaId, int viacaoId) throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<HorarioModel> list = new ArrayList<>();

        try {
            conn = ConnectionDAO.getConnection();
            String SQL = "SELECT * FROM horarios WHERE horario_id In (SELECT horario_id FROM horarios_das_viagens WHERE viagem_id In (SELECT viagem_id FROM viagens WHERE cidade_origem_id=? and cidade_destino_id=? and rodoviaria_id=? and viacao_id=?))";
            ps = conn.prepareStatement(SQL);

            ps.setInt(1, cidadeOrigemId);
            ps.setInt(2, cidadeDestinoId);
            ps.setInt(3, rodoviariaId);
            ps.setInt(4, viacaoId);

            rs = ps.executeQuery();
            while (rs.next()) {
                HorarioModel horarios = new HorarioModel(rs.getInt(1), rs.getTime(2), rs.getTime(3));
                list.add(horarios);
            }
        } catch (SQLException sqle) {
            throw new Exception("Erro ao buscar todos: \n" + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return list;
    }

}
