package DAOs;

import Models.HorarioDaViagemModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HorarioDaViagemDAO implements DAO {

    @Override
    public void atualizar(Object ob) throws Exception {
        HorarioDaViagemModel vModel = (HorarioDaViagemModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("UPDATE horarios_das_viagens SET viagem_id = ?, horario_id=? WHERE viagem_id=? and horario_id=?");
            ps.setInt(1, vModel.getViagemId());
            ps.setInt(2, vModel.getHorarioId());
            ps.setInt(3, vModel.getViagemId());
            ps.setInt(4, vModel.getHorarioId());

            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao atualizar dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    @Override
    public void excluir(Object ob) throws Exception {
        HorarioDaViagemModel vModel = (HorarioDaViagemModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("DELETE FROM horarios_das_viagens WHERE viagem_id=? and horario_id=?");

            ps.setInt(1, vModel.getViagemId());
            ps.setInt(2, vModel.getHorarioId());
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao excluir dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    @Override
    public List listaTodos() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<HorarioDaViagemModel> list = new ArrayList<>();

        try {
            String SQL = "SELECT * FROM horarios_das_viagens";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);
            rs = ps.executeQuery();

            while (rs.next()) {
                HorarioDaViagemModel horariosDasViagens = new HorarioDaViagemModel(rs.getInt(1), rs.getInt(2));
                list.add(horariosDasViagens);
            }
        } catch (SQLException sqle) {
            throw new Exception("Erro ao buscar todos: \n" + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return list;
    }

    @Override
    public List procura(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void salvar(Object ob) throws Exception {
        HorarioDaViagemModel vModel = (HorarioDaViagemModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            String SQL = "INSERT INTO horarios_das_viagens (viagem_id, horario_id) value (?, ?)";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);

            ps.setInt(1, vModel.getViagemId());
            ps.setInt(2, vModel.getHorarioId());

            ps.executeUpdate();
        } catch (SQLException sqle) {
            System.out.println(sqle);
            throw new Exception("Erro ao inserir dados: \n" + sqle);

        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

}
