package DAOs;

import Models.RodoviariaModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RodoviariaDAO implements DAO {

    @Override
    public void atualizar(Object ob) throws Exception {
        RodoviariaModel vModel = (RodoviariaModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("UPDATE rodoviarias SET rodoviaria_nome=?, rodoviaria_endereco=?, rodoviaria_telefone=?, rodoviaria_horario_atendimento=?, cidade_id=? WHERE rodoviaria_id=?");
            ps.setString(1, vModel.getRodoviariaNome());
            ps.setString(2, vModel.getRodoviariaEndereco());
            ps.setString(3, vModel.getRodoviariaTelefone());
            ps.setString(4, vModel.getRodoviariaHorarioAtendimento());
            ps.setInt(5, vModel.getCidadeId());
            ps.setInt(6, vModel.getRodoviariaId());

            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao atualizar dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    @Override
    public void excluir(Object ob) throws Exception {
        RodoviariaModel vModel = (RodoviariaModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("DELETE FROM rodoviarias WHERE rodoviaria_id=?");

            ps.setInt(1, vModel.getRodoviariaId());
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao excluir dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    @Override
    public List listaTodos() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<RodoviariaModel> list = new ArrayList<>();

        try {
            String SQL = "SELECT * FROM rodoviarias";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);
            rs = ps.executeQuery();

            while (rs.next()) {
                RodoviariaModel rodoviarias = new RodoviariaModel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6));
                list.add(rodoviarias);
            }
        } catch (SQLException sqle) {
            throw new Exception("Erro ao buscar todos: \n" + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return list;
    }

    public List listaTodasRodoviariasPorCidade(int idCidade) throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<RodoviariaModel> list = new ArrayList<>();

        try {
            String SQL = "SELECT * FROM rodoviarias WHERE cidade_id = ?";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);

            ps.setInt(1, idCidade);
            rs = ps.executeQuery();

            while (rs.next()) {
                RodoviariaModel rodoviarias = new RodoviariaModel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6));
                list.add(rodoviarias);
            }
        } catch (SQLException sqle) {
            throw new Exception("Erro ao buscar todos: \n" + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return list;
    }

    @Override
    public List procura(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void salvar(Object ob) throws Exception {
        RodoviariaModel vModel = (RodoviariaModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            String SQL = "INSERT INTO rodoviarias (rodoviaria_id, rodoviaria_nome, rodoviaria_endereco, rodoviaria_telefone, rodoviaria_horario_atendimento, cidade_id) value (null, ?, ?, ?, ?, ?)";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);

            ps.setString(1, vModel.getRodoviariaNome());
            ps.setString(2, vModel.getRodoviariaEndereco());
            ps.setString(3, vModel.getRodoviariaTelefone());
            ps.setString(4, vModel.getRodoviariaHorarioAtendimento());
            ps.setInt(5, vModel.getCidadeId());

            System.out.println(ps);

            ps.executeUpdate();
        } catch (SQLException sqle) {
            System.out.println(sqle);
            throw new Exception("Erro ao inserir dados: \n" + sqle);

        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }
}
