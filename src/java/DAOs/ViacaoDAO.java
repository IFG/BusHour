package DAOs;

import Models.ViacaoModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ViacaoDAO implements DAO {
    
    @Override
    public void atualizar(Object ob) throws Exception {
        ViacaoModel vModel = (ViacaoModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;
        
        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }
        
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("UPDATE viacoes SET viacao_nome = ?, viacao_telefone=? WHERE viacao_id=?");
            ps.setString(1, vModel.getViacaoNome());
            ps.setString(2, vModel.getViacaoTelefone());
            ps.setInt(3, vModel.getViacaoId());
            
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao atualizar dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }
    
    @Override
    public void excluir(Object ob) throws Exception {
        ViacaoModel vModel = (ViacaoModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;
        
        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }
        
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("DELETE FROM viacoes WHERE viacao_id=?");
            
            ps.setInt(1, vModel.getViacaoId());
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao excluir dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }
    
    @Override
    public List listaTodos() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<ViacaoModel> list = new ArrayList<>();
        
        try {
            String SQL = "SELECT * FROM viacoes";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                ViacaoModel viacoes = new ViacaoModel(rs.getInt(1), rs.getString(2), rs.getString(4));
                list.add(viacoes);
            }
        } catch (SQLException sqle) {
            throw new Exception("Erro ao buscar todos: \n" + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return list;
    }
    
    public List listaTodosPorRodoviaria(int rodoviariaId) throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<ViacaoModel> list = new ArrayList<>();
        
        try {
            String SQL = "SELECT * FROM viacoes WHERE viacao_id In (SELECT viacao_id FROM viacoes_das_rodoviarias WHERE rodoviaria_id=?)";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);
            
            ps.setInt(1, rodoviariaId);
            
            rs = ps.executeQuery();            
            while (rs.next()) {
                ViacaoModel viacoes = new ViacaoModel(rs.getInt(1), rs.getString(2), rs.getString(4));
                list.add(viacoes);
            }
        } catch (SQLException sqle) {
            throw new Exception("Erro ao buscar todos: \n" + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return list;
    }
    
    @Override
    public List procura(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void salvar(Object ob) throws Exception {
        ViacaoModel vModel = (ViacaoModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;
        
        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }
        
        try {
            String SQL = "insert into viacoes (viacao_id, viacao_nome, viacao_historico, viacao_telefone) value (null, ?, ?, ?)";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);
            
            ps.setString(1, vModel.getViacaoNome());
            ps.setString(2, vModel.getViacaoHistorico());
            ps.setString(3, vModel.getViacaoTelefone());
            
            ps.executeUpdate();
        } catch (SQLException sqle) {
            System.out.println(sqle);
            throw new Exception("Erro ao inserir dados: \n" + sqle);
            
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }
    
}
