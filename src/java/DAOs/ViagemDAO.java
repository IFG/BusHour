package DAOs;

import Models.ViagemModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ViagemDAO implements DAO {

    @Override
    public void atualizar(Object ob) throws Exception {
        ViagemModel vModel = (ViagemModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("UPDATE viagens SET cidade_origem_id=?, cidade_destino_id=?, rodoviaria_id=?, viacao_id=? WHERE viagem_id=?");
            ps.setInt(1, vModel.getCidadeOrigemId());
            ps.setInt(2, vModel.getCidadeDestinoId());
            ps.setInt(3, vModel.getRodoviariaId());
            ps.setInt(4, vModel.getViacaoId());
            ps.setInt(5, vModel.getViagemId());

            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao atualizar dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    @Override
    public void excluir(Object ob) throws Exception {
        ViagemModel vModel = (ViagemModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("DELETE FROM viagens WHERE viagem_id=?");

            ps.setInt(1, vModel.getRodoviariaId());
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new Exception("Erro ao excluir dados: " + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }

    @Override
    public List listaTodos() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<ArrayList> list = new ArrayList<>();

        try {
            String SQL = "SELECT viagem_id, cidades_origem.cidade_nome As cidade_origem, cidades_destino.cidade_nome As cidade_destino, rodoviaria_nome, viacao_nome FROM viagens INNER JOIN cidades as cidades_origem ON viagens.cidade_origem_id = cidades_origem.cidade_id INNER JOIN cidades as cidades_destino ON viagens.cidade_destino_id = cidades_destino.cidade_id INNER JOIN rodoviarias ON viagens.rodoviaria_id = rodoviarias.rodoviaria_id INNER JOIN viacoes ON viagens.viacao_id = viacoes.viacao_id";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);
            rs = ps.executeQuery();

            while (rs.next()) {
                ArrayList<String> viagens = new ArrayList<>();

                viagens.add(String.valueOf(rs.getInt(1)));
                viagens.add(rs.getString(2));
                viagens.add(rs.getString(3));
                viagens.add(rs.getString(4));
                viagens.add(rs.getString(5));

                list.add(viagens);
            }
        } catch (SQLException sqle) {
            throw new Exception("Erro ao buscar todos: \n" + sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return list;
    }

    @Override
    public List procura(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void salvar(Object ob) throws Exception {
        ViagemModel vModel = (ViagemModel) ob;
        PreparedStatement ps = null;
        Connection conn = null;

        if (vModel == null) {
            throw new Exception("O valor passado não pode ser nulo");
        }

        try {
            String SQL = "INSERT INTO viagens (viagem_id, cidade_origem_id, cidade_destino_id, rodoviaria_id, viacao_id) value (null, ?, ?, ?, ?)";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL);

            ps.setInt(1, vModel.getCidadeOrigemId());
            ps.setInt(2, vModel.getCidadeDestinoId());
            ps.setInt(3, vModel.getRodoviariaId());
            ps.setInt(4, vModel.getViacaoId());

            ps.executeUpdate();
        } catch (SQLException sqle) {
            System.out.println(sqle);
            throw new Exception("Erro ao inserir dados: \n" + sqle);

        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }
}
