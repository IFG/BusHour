package Models;

public class CidadeModel {

    private int cidadeId;
    private String cidadeNome;
    private String cidadeAtivo;
    private int estadoId;

    public CidadeModel(int cidadeId, String cidadeNome, String cidadeAtivo, int estadoId) {
        this.cidadeId = cidadeId;
        this.cidadeNome = cidadeNome;
        this.cidadeAtivo = cidadeAtivo;
        this.estadoId = estadoId;
    }

    public int getCidadeId() {
        return cidadeId;
    }

    public void setCidadeId(int cidadeId) {
        this.cidadeId = cidadeId;
    }

    public String getCidadeNome() {
        return cidadeNome;
    }

    public void setCidadeNome(String cidadeNome) {
        this.cidadeNome = cidadeNome;
    }

    public String getCidadeAtivo() {
        return cidadeAtivo;
    }

    public void setCidadeAtivo(String cidadeAtivo) {
        this.cidadeAtivo = cidadeAtivo;
    }

    public int getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(int estadoId) {
        this.estadoId = estadoId;
    }

}
