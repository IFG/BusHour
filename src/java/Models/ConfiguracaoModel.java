package Models;

public class ConfiguracaoModel {

    private int configuracoesId;
    private int estadoId;
    private int cidadeId;
    private int rodoviariaId;

    public ConfiguracaoModel(int configuracoesId, int estadoId, int cidadeId, int rodoviariaId) {
        this.configuracoesId = configuracoesId;
        this.estadoId = estadoId;
        this.cidadeId = cidadeId;
        this.rodoviariaId = rodoviariaId;
    }

    public int getConfiguracoesId() {
        return configuracoesId;
    }

    public void setConfiguracoesId(int configuracoesId) {
        this.configuracoesId = configuracoesId;
    }

    public int getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(int estadoId) {
        this.estadoId = estadoId;
    }

    public int getCidadeId() {
        return cidadeId;
    }

    public void setCidadeId(int cidadeId) {
        this.cidadeId = cidadeId;
    }

    public int getRodoviariaId() {
        return rodoviariaId;
    }

    public void setRodoviariaId(int rodoviariaId) {
        this.rodoviariaId = rodoviariaId;
    }

}
