package Models;

public class EstadoModel {

    private int estadoId;
    private String estadoNome;
    private String estadoAtivo;

    public EstadoModel(int estadoId, String estadoNome, String estadoAtivo) {
        this.estadoId = estadoId;
        this.estadoNome = estadoNome;
        this.estadoAtivo = estadoAtivo;
    }

    public int getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(int estadoId) {
        this.estadoId = estadoId;
    }

    public String getEstadoNome() {
        return estadoNome;
    }

    public void setEstadoNome(String estadoNome) {
        this.estadoNome = estadoNome;
    }

    public String getEstadoAtivo() {
        return estadoAtivo;
    }

    public void setEstadoAtivo(String estadoAtivo) {
        this.estadoAtivo = estadoAtivo;
    }

}
