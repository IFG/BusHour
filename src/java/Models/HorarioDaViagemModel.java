package Models;

public class HorarioDaViagemModel {

    private int viagemId;
    private int horarioId;

    public HorarioDaViagemModel(int viagemId, int horarioId) {
        this.viagemId = viagemId;
        this.horarioId = horarioId;
    }

    public int getViagemId() {
        return viagemId;
    }

    public void setViagemId(int viagemId) {
        this.viagemId = viagemId;
    }

    public int getHorarioId() {
        return horarioId;
    }

    public void setHorarioId(int horarioId) {
        this.horarioId = horarioId;
    }

}
