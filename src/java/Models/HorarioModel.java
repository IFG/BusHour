package Models;

import java.sql.Time;

public class HorarioModel {

    private int horarioId;
    private Time horarioSaida;
    private Time horarioChegada;

    public HorarioModel(int horarioId, Time horarioSaida, Time horarioChegada) {
        this.horarioId = horarioId;
        this.horarioSaida = horarioSaida;
        this.horarioChegada = horarioChegada;
    }

    public int getHorarioId() {
        return horarioId;
    }

    public void setHorarioId(int horarioId) {
        this.horarioId = horarioId;
    }

    public Time getHorarioSaida() {
        return horarioSaida;
    }

    public void setHorarioSaida(Time horarioSaida) {
        this.horarioSaida = horarioSaida;
    }

    public Time getHorarioChegada() {
        return horarioChegada;
    }

    public void setHorarioChegada(Time horarioChegada) {
        this.horarioChegada = horarioChegada;
    }

}
