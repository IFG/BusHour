package Models;

public class RodoviariaModel {

    private int rodoviariaId;
    private String rodoviariaNome;
    private String rodoviariaEndereco;
    private String rodoviariaTelefone;
    private String rodoviariaHorarioAtendimento;
    private int cidadeId;

    public RodoviariaModel(int rodoviariaId, String rodoviariaNome, String rodoviariaEndereco, String rodoviariaTelefone, String rodoviariaHorarioAtendimento, int cidadeId) {
        this.rodoviariaId = rodoviariaId;
        this.rodoviariaNome = rodoviariaNome;
        this.rodoviariaEndereco = rodoviariaEndereco;
        this.rodoviariaTelefone = rodoviariaTelefone;
        this.rodoviariaHorarioAtendimento = rodoviariaHorarioAtendimento;
        this.cidadeId = cidadeId;
    }

    public int getRodoviariaId() {
        return rodoviariaId;
    }

    public void setRodoviariaId(int rodoviariaId) {
        this.rodoviariaId = rodoviariaId;
    }

    public String getRodoviariaNome() {
        return rodoviariaNome;
    }

    public void setRodoviariaNome(String rodoviariaNome) {
        this.rodoviariaNome = rodoviariaNome;
    }

    public String getRodoviariaEndereco() {
        return rodoviariaEndereco;
    }

    public void setRodoviariaEndereco(String rodoviariaEndereco) {
        this.rodoviariaEndereco = rodoviariaEndereco;
    }

    public String getRodoviariaTelefone() {
        return rodoviariaTelefone;
    }

    public void setRodoviariaTelefone(String rodoviariaTelefone) {
        this.rodoviariaTelefone = rodoviariaTelefone;
    }

    public String getRodoviariaHorarioAtendimento() {
        return rodoviariaHorarioAtendimento;
    }

    public void setRodoviariaHorarioAtendimento(String rodoviariaHorarioAtendimento) {
        this.rodoviariaHorarioAtendimento = rodoviariaHorarioAtendimento;
    }

    public int getCidadeId() {
        return cidadeId;
    }

    public void setCidadeId(int cidadeId) {
        this.cidadeId = cidadeId;
    }

}
