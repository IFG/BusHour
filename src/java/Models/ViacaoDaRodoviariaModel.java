package Models;

public class ViacaoDaRodoviariaModel {

    private int viacaoId;
    private int rodoviariaId;

    public ViacaoDaRodoviariaModel(int viacaoId, int rodoviariaId) {
        this.viacaoId = viacaoId;
        this.rodoviariaId = rodoviariaId;
    }

    public int getViacaoId() {
        return viacaoId;
    }

    public void setViacaoId(int viacaoId) {
        this.viacaoId = viacaoId;
    }

    public int getRodoviariaId() {
        return rodoviariaId;
    }

    public void setRodoviariaId(int rodoviariaId) {
        this.rodoviariaId = rodoviariaId;
    }

}
