package Models;

public class ViacaoModel {

    private int viacaoId;
    private String viacaoNome;
    private String viacaoHistorico;
    private String viacaoTelefone;
    
    public ViacaoModel(int viacaoId, String viacaoNome, String viacaoTelefone) {
        this.viacaoId = viacaoId;
        this.viacaoNome = viacaoNome;
        this.viacaoTelefone = viacaoTelefone;
    }
    public ViacaoModel(int viacaoId, String viacaoNome, String viacaoHistorico, String viacaoTelefone) {
        this.viacaoId = viacaoId;
        this.viacaoNome = viacaoNome;
        this.viacaoHistorico = viacaoHistorico;
        this.viacaoTelefone = viacaoTelefone;
    }

    public int getViacaoId() {
        return viacaoId;
    }

    public void setViacaoId(int viacaoId) {
        this.viacaoId = viacaoId;
    }

    public String getViacaoNome() {
        return viacaoNome;
    }

    public void setViacaoNome(String viacaoNome) {
        this.viacaoNome = viacaoNome;
    }

    public String getViacaoHistorico() {
        return viacaoHistorico;
    }

    public void setViacaoHistorico(String viacaoHistorico) {
        this.viacaoHistorico = viacaoHistorico;
    }

    public String getViacaoTelefone() {
        return viacaoTelefone;
    }

    public void setViacaoTelefone(String viacaoTelefone) {
        this.viacaoTelefone = viacaoTelefone;
    }

}
