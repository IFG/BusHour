package Models;

public class ViagemModel {

    private int viagemId;
    private int cidadeOrigemId;
    private int cidadeDestinoId;
    private int rodoviariaId;
    private int viacaoId;

    public ViagemModel(int viagemId, int cidadeOrigemId, int cidadeDestinoId, int rodoviariaId, int viacaoId) {
        this.viagemId = viagemId;
        this.cidadeOrigemId = cidadeOrigemId;
        this.cidadeDestinoId = cidadeDestinoId;
        this.rodoviariaId = rodoviariaId;
        this.viacaoId = viacaoId;
    }

    public int getViagemId() {
        return viagemId;
    }

    public void setViagemId(int viagemId) {
        this.viagemId = viagemId;
    }

    public int getCidadeOrigemId() {
        return cidadeOrigemId;
    }

    public void setCidadeOrigemId(int cidadeOrigemId) {
        this.cidadeOrigemId = cidadeOrigemId;
    }

    public int getCidadeDestinoId() {
        return cidadeDestinoId;
    }

    public void setCidadeDestinoId(int cidadeDestinoId) {
        this.cidadeDestinoId = cidadeDestinoId;
    }

    public int getRodoviariaId() {
        return rodoviariaId;
    }

    public void setRodoviariaId(int rodoviariaId) {
        this.rodoviariaId = rodoviariaId;
    }

    public int getViacaoId() {
        return viacaoId;
    }

    public void setViacaoId(int viacaoId) {
        this.viacaoId = viacaoId;
    }

}
