<%-- 
    Document   : listaJSON
    Created on : 08/11/2017, 17:22:48
    Author     : tatuapu
--%>

<%@page import="DAOs.RodoviariaDAO"%>
<%@page import="Models.ViagemModel"%>
<%@page import="javafx.scene.control.Alert"%>
<%@page import="Models.HorarioModel"%>
<%@page import="Models.RodoviariaModel"%>
<%@page import="Models.CidadeModel"%>
<%@page import="Models.EstadoModel"%>
<%@page import="Models.ViacaoModel"%>
<%@page import="java.io.StringWriter"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.json.simple.*"%>
<%
    JSONObject dados = new JSONObject();
    String classe = (request.getAttribute("classe") != null) ? request.getAttribute("classe").toString() : "";
    if (classe.equals("estados")) {
        ArrayList<EstadoModel> lista;
        lista = (ArrayList<EstadoModel>) request.getAttribute("lista-estados");
        JSONObject pai = new JSONObject();
        JsonArray filhos = new JsonArray();

        for (EstadoModel cr : lista) {
            dados = new JSONObject();
            dados.put("ativoEstado", cr.getEstadoAtivo());
            dados.put("nomeEstado", cr.getEstadoNome());
            dados.put("idEstado", cr.getEstadoId());
            filhos.add(dados);
        }
        pai.put("data", filhos);
        out.print(pai);

    } else if (classe.equals("viacoes")) {
        ArrayList<ViacaoModel> lista;
        lista = (ArrayList<ViacaoModel>) request.getAttribute("lista-viacoes");
        JSONObject pai = new JSONObject();
        JsonArray filhos = new JsonArray();
        for (ViacaoModel cr : lista) {

            dados = new JSONObject();
            dados.put("telefoneViacao", cr.getViacaoTelefone());
            dados.put("nomeViacao", cr.getViacaoNome());
            dados.put("idViacao", cr.getViacaoId());

            filhos.add(dados);

        }
        pai.put("data", filhos);
        out.print(pai);
    } else if (classe.equals("cidades")) {
        ArrayList<CidadeModel> lista;
        lista = (ArrayList<CidadeModel>) request.getAttribute("lista-cidades");
        JSONObject pai = new JSONObject();
        JsonArray filhos = new JsonArray();
        for (CidadeModel cr : lista) {

            dados = new JSONObject();
            dados.put("idEstado", cr.getEstadoId());
            dados.put("ativoCidade", cr.getCidadeAtivo());
            dados.put("nomeCidade", cr.getCidadeNome());
            dados.put("idCidade", cr.getCidadeId());

            filhos.add(dados);

        }
        pai.put("data", filhos);
        out.print(pai);
    } else if (classe.equals("rodoviarias")) {
        ArrayList<RodoviariaModel> lista;
        lista = (ArrayList<RodoviariaModel>) request.getAttribute("lista-rodoviarias");
        JSONObject pai = new JSONObject();
        JsonArray filhos = new JsonArray();
        for (RodoviariaModel cr : lista) {

            dados = new JSONObject();
            dados.put("idCidade", cr.getCidadeId());
            dados.put("nomeRodoviaria", cr.getRodoviariaNome());
            dados.put("enderecoRodoviaria", cr.getRodoviariaEndereco());
            dados.put("telefoneRodoviaria", cr.getRodoviariaTelefone());
            dados.put("horarioRodoviaria", cr.getRodoviariaHorarioAtendimento());
            dados.put("idRodoviaria", cr.getRodoviariaId());

            filhos.add(dados);

        }
        pai.put("data", filhos);
        out.print(pai);

    } else if (classe.equals("viacoes")) {
        ArrayList<ViacaoModel> lista;
        lista = (ArrayList<ViacaoModel>) request.getAttribute("lista-viacoes");
        JSONObject pai = new JSONObject();
        JsonArray filhos = new JsonArray();

        for (ViacaoModel cr : lista) {
            dados = new JSONObject();
            dados.put("idViacao", cr.getViacaoId());
            dados.put("nomeViacao", cr.getViacaoNome());
            dados.put("historicoViacao", cr.getViacaoHistorico());
            dados.put("telefoneViacao", cr.getViacaoTelefone());

            filhos.add(dados);
        }
        pai.put("data", filhos);
        out.print(pai);
    } else if (classe.equals("cidadedestino")) {
        ArrayList<CidadeModel> lista;
        lista = (ArrayList<CidadeModel>) request.getAttribute("lista-cidadedestino");
        JSONObject pai = new JSONObject();
        JsonArray filhos = new JsonArray();

        for (CidadeModel cr : lista) {
            dados = new JSONObject();
            dados.put("idEstado", cr.getEstadoId());
            dados.put("ativoCidade", cr.getCidadeAtivo());
            dados.put("nomeCidade", cr.getCidadeNome());
            dados.put("idCidade", cr.getCidadeId());

            filhos.add(dados);
        }

        pai.put("data", filhos);
        out.print(pai);
    } else if (classe.equals("horarios")) {
        ArrayList<HorarioModel> lista;
        lista = (ArrayList<HorarioModel>) request.getAttribute("lista-horarios");
        JSONObject pai = new JSONObject();
        JsonArray filhos = new JsonArray();

        for (HorarioModel cr : lista) {
            dados = new JSONObject();
            dados.put("idHorario", cr.getHorarioId());
            dados.put("saidaHorario", cr.getHorarioSaida());
            dados.put("chegdaHorario", cr.getHorarioChegada());
            filhos.add(dados);
        }

        pai.put("data", filhos);
        out.print(pai);
    } else if (classe.equals("horarios2")) {
        ArrayList<HorarioModel> lista;
        lista = (ArrayList<HorarioModel>) request.getAttribute("lista-horarios");
        JSONObject pai = new JSONObject();
        JsonArray filhos = new JsonArray();

        for (HorarioModel cr : lista) {
            dados = new JSONObject();
            dados.put("idHorario", cr.getHorarioId());
            dados.put("nomeHorario", "Sa�da: " + cr.getHorarioSaida() + " - Chegada: " + cr.getHorarioChegada());
            filhos.add(dados);
        }

        pai.put("data", filhos);
        out.print(pai);
    } else if (classe.equals("viagens")) {
        ArrayList<ArrayList> lista;
        lista = (ArrayList<ArrayList>) request.getAttribute("lista-viagem");
        JSONObject pai = new JSONObject();
        JsonArray filhos = new JsonArray();

        //RodoviariaDAO dao = new RodoviariaDAO();
        for (int i = 0; i < lista.size(); i++) {
            dados = new JSONObject();
            dados.put("idViagem", lista.get(i).get(0));
            dados.put("nomeViagem", "De: " + lista.get(i).get(1) + " - Para: " + lista.get(i).get(2) + " - Rodovi�ria: " + lista.get(i).get(3) + " - Via��o: " + lista.get(i).get(4));
            filhos.add(dados);
        }

        pai.put("data", filhos);
        out.print(pai);
    }
%>
