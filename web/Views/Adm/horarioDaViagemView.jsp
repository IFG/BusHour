<%-- 
    Document   : horarioDaViagemView
    Created on : 23/11/2017, 08:11:58
    Author     : Cayo Eduardo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>BusHour</title>

        <!-- Bootstrap core CSS -->
        <link href="<%=request.getContextPath()%>/CSS/bootstrap.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/CSS/font-awesome.css" rel="stylesheet">

        <!-- Custom fonts for this template -->
        <!--<link href="<%=request.getContextPath()%>/Bootstrap/vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">-->
        <!--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">-->
        <!--<link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>-->
        <!--<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>-->
        <!--<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>-->


        <!-- Custom styles for this template -->
        <link href="<%=request.getContextPath()%>/CSS/estilo.css" rel="stylesheet">

        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
        <!-- Bootstrap core JavaScript -->                
        <script src="<%=request.getContextPath()%>/JS/jquery.js"></script>
        <script src="<%=request.getContextPath()%>/JS/bootstrap.bundle.js"></script>

        <!-- Custom scripts for this template -->
        <!--<script src="<%=request.getContextPath()%>/JS/javascript.js"></script>-->
        <script src="<%=request.getContextPath()%>/Scripts/jquery.validate.min.js"></script>
    </head>

    <body id="page-top">

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container-fluid">
                <a class="navbar-brand js-scroll-trigger" href="administradorView.jsp"><i class="fa fa-bus"></i> BusHour</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fa fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ml-auto">
                        <li class="nav-item fa">
                            <a class="nav-link js-scroll-trigger" href="estadoView.jsp"><i class="fa fa-bus"></i> Estados</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="cidadeView.jsp"><i class="fa fa-bus"></i> Cidades</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="rodoviariaView.jsp"><i class="fa fa-bus"></i> Rodoviárias</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="viacaoView.jsp"><i class="fa fa-bus"></i> Viações</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="viacaoDaRodoviariaView.jsp"><i class="fa fa-bus"></i> Viação das Rodoviárias</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="horarioView.jsp"><i class="fa fa-bus"></i> Horários</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="viagemView.jsp"><i class="fa fa-bus"></i> Viagens</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="horarioDaViagemView.jsp"><i class="fa fa-bus"></i> Horário das Viagens</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Header -->
        <header class="masthead" style="text-align: left">
            <form action="" id="formCadastro" method="post">
                <div class="container">
                    <div class="intro-text">
                        <center><b><h3>Horarios das Viagens</h3></b></center>

                        <div class="col-sm-12">
                            <b><label>Viagem</label></b>
                            <select id="viagemId" name="viagemId" class="form-control" style="background-color: #212529;color: #fed136">
                                <option value="-1">Escolha uma Opção</option>
                            </select>
                        </div>

                        <div class="col-sm-12">
                            <b><label>Horário</label></b>
                            <select id="horarioId" name="horarioId" class="form-control" style="background-color: #212529;color: #fed136">
                                <option value="-1">Escolha uma Opção</option>
                            </select>
                        </div>

                        <div class="col-sm-12" style="text-align: center;margin-top: 10px">
                            <button type="submit" onclick="salvar()" class="btn btn-primary"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </div>
                </div>  
            </form>
        </header>   
        <script>
            //submetendo um formulario com jquery!!
            function salvar() {
                if (document.getElementById("viagemId").selectedIndex == "") {
                    alert("Selecione uma Viagem");
                } else {
                    if (document.getElementById("horarioId").selectedIndex == "") {
                        alert("Selecione um Horário");
                    } else {
                        $("#formCadastro").attr('action', '../../cadastrarHorarioDaViagem');
                        $("#formCadastro").submit();
                    }
                }
            }

            function resetaCombo(el) {
                $("select[name='" + el + "']").empty();//retira os elementos antigos
                var option = document.createElement('option');
                $(option).attr({value: '0'});
                $(option).append('Escolha uma Opção');
                $("select[name='" + el + "']").append(option);
            }

            $(document).ready(function () {
                $.ajax({
                    type: 'GET',
                    url: '<%=request.getContextPath()%>/getDados?cmd=listaviagem',
                    data: 'data',
                    mimeType: 'json',
                    success: function (retorno) {
                        var option = new Array();
                        var dados = retorno.data;
                        $.each(dados, function (i, dados) {
                            option[i] = document.createElement('option');//criando o option
                            $(option[i]).attr({value: dados.idViagem});//colocando o value no option
                            $(option[i]).append(dados.nomeViagem);//colocando o 'label'
                            $("#viagemId").append(option[i]);//jogando um à um os options no próximo combo
                        });

                        filtraHorarios();
                    },
                    error: function () {
                        alert('Fail!');
                    }
                });
            });

            function filtraHorarios() {
                $.ajax({
                    type: 'GET',
                    url: '<%=request.getContextPath()%>/getDados?cmd=listahorario2',
                    data: 'data',
                    mimeType: 'json',
                    success: function (retorno) {
                        var option = new Array();
                        var dados = retorno.data;
                        $.each(dados, function (i, dados) {
                            option[i] = document.createElement('option');//criando o option
                            $(option[i]).attr({value: dados.idHorario});//colocando o value no option
                            $(option[i]).append(dados.nomeHorario);//colocando o 'label'
                            $("#horarioId").append(option[i]);//jogando um à um os options no próximo combo
                        });
                    },
                    error: function () {
                        alert('Fail!');
                    }
                });
            }
        </script>
    </body>
</html>
