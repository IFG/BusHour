<%-- 
    Document   : PainelControle
    Created on : 16/11/2017, 08:25:37
    Author     : Daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://markcell.github.io/jquery-tabledit/assets/css/bootstrap-yeti.min.css" rel="stylesheet" id="theme-file">
        <link href="https://markcell.github.io/jquery-tabledit/assets/css/prettify.min.css" rel="stylesheet">
        <link href="https://markcell.github.io/jquery-tabledit/assets/css/prettify-bootstrap.min.css" rel="stylesheet">
        <link href="https://markcell.github.io/jquery-tabledit/assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://markcell.github.io/jquery-tabledit/assets/css/custom.css" rel="stylesheet">
        <title>JSP Page</title>
    </head>
    <body>
        <h2>Viações</h2>
        <div class="table-responsive">
            <table class="table table-striped table-bordered" id="tabela-viacoes">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nome</th>
                        <th>Telefone</th>
                        <th class="tabledit-toolbar-column"></th></tr>
                </thead>
                <tbody id="tbody">     
                </tbody>
            </table>
        </div>
        <script src="https://markcell.github.io/jquery-tabledit/assets/js/jquery.min.js"></script>
        <script src="https://markcell.github.io/jquery-tabledit/assets/js/bootstrap.min.js"></script>
        <script src="https://markcell.github.io/jquery-tabledit/assets/js/prettify.min.js"></script>
        <script src="https://markcell.github.io/jquery-tabledit/assets/js/tabledit.min.js"></script>

        <script>
            $(document).ready(function () {
                $.ajax({
                    type: 'GET',
                    url: '../../getDados?cmd=listaviacoes',
                    data: 'data',
                    mimeType: 'json',
                    success: function (retorno) {
                        var dados = retorno.data;
                        $.each(dados, function (i, dados) {
                            var body = "<tr>";
                            body += "<td>" + dados.idViacao + "</td>";
                            body += "<td>" + dados.nomeViacao + "</td>";
                            body += "<td>" + dados.telefoneViacao + "</td>";
                            body += "</tr>";
                            $("#tbody").append(body);
                        });
                        montaTabela();
                        /*DataTables instantiation.*/
                        $("#tabela-viacoes").DataTable();
                    },
                    error: function () {
                        alert('Fail!');
                    }
                });
            });

            function montaTabela() {
                $('#tabela-viacoes').Tabledit({
                    url: '../../dados',
                    editButton: true,
                    columns: {
                        identifier: [0, 'idViacao'],
                        editable: [[1, 'nomeViacao'], [2, 'telefoneViacao']]
                    },
                    onDraw: function () {
                        console.log('onDraw()');
                    },
                    onSuccess: function (data, textStatus, jqXHR) {
                        console.log('onSuccess(data, textStatus, jqXHR)');
                        console.log(data);
                        console.log(textStatus);
                        console.log(jqXHR);
                    },
                    onFail: function (jqXHR, textStatus, errorThrown) {
                        console.log('onFail(jqXHR, textStatus, errorThrown)');
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                    },
                    onAlways: function () {
                        console.log('onAlways()');
                    },
                    onAjax: function (action, serialize) {
                        console.log('onAjax(action, serialize)');
                        console.log(action);
                        console.log(serialize);
                    }
                });
            }
        </script>
    </body>
</html>
