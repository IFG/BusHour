<%-- 
    Document   : viagemView
    Created on : 23/11/2017, 08:10:59
    Author     : Cayo Eduardo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>BusHour</title>

        <!-- Bootstrap core CSS -->
        <link href="<%=request.getContextPath()%>/CSS/bootstrap.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/CSS/font-awesome.css" rel="stylesheet">

        <!-- Custom fonts for this template -->
        <!--<link href="<%=request.getContextPath()%>/Bootstrap/vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">-->
        <!--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">-->
        <!--<link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>-->
        <!--<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>-->
        <!--<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>-->


        <!-- Custom styles for this template -->
        <link href="<%=request.getContextPath()%>/CSS/estilo.css" rel="stylesheet">

        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
        <!-- Bootstrap core JavaScript -->                
        <script src="<%=request.getContextPath()%>/JS/jquery.js"></script>
        <script src="<%=request.getContextPath()%>/JS/bootstrap.bundle.js"></script>

        <!-- Custom scripts for this template -->
        <!--<script src="<%=request.getContextPath()%>/JS/javascript.js"></script>-->
        <script src="<%=request.getContextPath()%>/Scripts/jquery.validate.min.js"></script>
    </head>

    <body id="page-top">

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container-fluid">
                <a class="navbar-brand js-scroll-trigger" href="administradorView.jsp"><i class="fa fa-bus"></i> BusHour</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fa fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ml-auto">
                        <li class="nav-item fa">
                            <a class="nav-link js-scroll-trigger" href="estadoView.jsp"><i class="fa fa-bus"></i> Estados</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="cidadeView.jsp"><i class="fa fa-bus"></i> Cidades</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="rodoviariaView.jsp"><i class="fa fa-bus"></i> Rodoviárias</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="viacaoView.jsp"><i class="fa fa-bus"></i> Viações</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="viacaoDaRodoviariaView.jsp"><i class="fa fa-bus"></i> Viação das Rodoviárias</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="horarioView.jsp"><i class="fa fa-bus"></i> Horários</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="viagemView.jsp"><i class="fa fa-bus"></i> Viagens</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="horarioDaViagemView.jsp"><i class="fa fa-bus"></i> Horário das Viagens</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Header -->
        <header class="masthead" style="text-align: left">
            <form action="" id="formCadastro" method="post">
                <div class="container">
                    <div class="intro-text">
                        <center><b><h3>Viagens</h3></b></center>

                        <div class="col-sm-12 al">
                            <b><label>Cidade de Origem</label></b>
                            <select onchange="filtraRodoviarias()" id="cidadeOrigemId" name="cidadeOrigemId" class="form-control fa" style="background-color: #212529;color: #fed136">
                                <option value="-1">Escolha uma Opção</option>
                            </select>
                        </div>

                        <div class="col-sm-12 al">
                            <b><label>Cidade de Destino</label></b>
                            <select id="cidadeDestinoId" name="cidadeDestinoId" class="form-control fa" style="background-color: #212529;color: #fed136">
                                <option value="-1">Escolha uma Opção</option>
                            </select>
                        </div>

                        <div class="col-sm-12 al">
                            <b><label>Rodoviária</label></b>
                            <select onchange="filtraViacao()" id="rodoviariaId" name="rodoviariaId" class="form-control fa" style="background-color: #212529;color: #fed136">
                                <option value="-1">Escolha uma Opção</option>
                            </select>
                        </div>

                        <div class="col-sm-12 al">
                            <b><label>Viação</label></b>
                            <select id="viacaoId" name="viacaoId" class="form-control fa" style="background-color: #212529;color: #fed136;">
                                <option value="-1">Escolha uma Opção</option>
                            </select>
                        </div>

                        <div class="col-sm-12" style="text-align: center;margin-top: 10px">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </div>
                </div>  
            </form>
        </header>   
        <script>
            //submetendo um formulario com jquery!!
            $.validator.setDefaults({
                submitHandler: function () {
                    if (document.getElementById("cidadeOrigemId").selectedIndex == "") {
                        alert("Selecione uma Cidade de Origem");
                    } else {
                        if (document.getElementById("cidadeDestinoId").selectedIndex == "") {
                            alert("Selecione uma Cidade de Destino");
                        } else {
                            if (document.getElementById("rodoviariaId").selectedIndex == "") {
                                alert("Selecione uma Rodoviária");
                            } else {
                                if (document.getElementById("viacaoId").selectedIndex == "")
                                    alert("Selecione uma Viação");
                                else {
                                    $("#formCadastro").attr('action', '../../cadastrarViagem');
                                    $("#formCadastro").submit();
                                }
                            }
                        }
                    }
                }
            });
            // validate the comment form when it is submitted
            $("#formCadastro").validate({
//                rules: {
//                    cidadeOrigemId: {
//                        required: true,
//                        minlength: 3
//                    },
//                    cidadeDestinoId: {
//                        required: true,
//                        minlength: 3
//                    },
//                    rodoviariaId: {
//                        required: true,
//                        minlength: 3
//                    },
//                    viacaoId: {
//                        required: true,
//                        minlength: 3
//                    }},
//                messages: {
//                    cidadeOrigemId: {
//                        required: "Por favor, Informe o Nome da Rodoviária",
//                        minlength: "O Nome da Rodoviária deve ter pelo menos 3 caracteres"
//                    },
//                    cidadeDestinoId: {
//                        required: "Por favor, Informe o Endereço",
//                        minlength: "O Endereço deve ter pelo menos 3 caracteres"
//                    },
//                    rodoviariaId: {
//                        required: "Por favor, Informe o Telefone",
//                        minlength: "O Telefone deve ter pelo menos 3 caracteres"
//                    },
//                    viacaoId: {
//                        required: "Por favor, Informe o Horário de Atendimento",
//                        minlength: "O Horário do Atendimento deve ter pelo menos 3 caracteres"
//                    }
//                }
            });

            function resetaCombo(el) {
                $("select[name='" + el + "']").empty();//retira os elementos antigos
                var option = document.createElement('option');
                $(option).attr({value: '0'});
                $(option).append('Escolha uma Opção');
                $("select[name='" + el + "']").append(option);
            }

            $(document).ready(function () {
                $.ajax({
                    type: 'GET',
                    url: '<%=request.getContextPath()%>/getDados?cmd=listacidades2',
                    data: 'data',
                    mimeType: 'json',
                    success: function (retorno) {
                        var option = new Array();
                        var dados = retorno.data;
                        $.each(dados, function (i, dados) {
                            var body = "<tr>";
                            option[i] = document.createElement('option');//criando o option
                            $(option[i]).attr({value: dados.idCidade});//colocando o value no option
                            $(option[i]).append(dados.nomeCidade);//colocando o 'label'
                            $("#cidadeOrigemId").append(option[i]);//jogando um à um os options no próximo combo
                        });
                        $.each(dados, function (i, dados) {
                            var body = "<tr>";
                            option[i] = document.createElement('option');//criando o option
                            $(option[i]).attr({value: dados.idCidade});//colocando o value no option
                            $(option[i]).append(dados.nomeCidade);//colocando o 'label'
                            $("#cidadeDestinoId").append(option[i]);//jogando um à um os options no próximo combo
                        });
                    },
                    error: function () {
                        alert('Fail!');
                    }
                });
            });

            function filtraRodoviarias() {
                var idCidade = $('#cidadeOrigemId').val();

                $.ajax({
                    type: 'GET',
                    url: '<%=request.getContextPath()%>/getDados?cmd=listarodoviarias&idCidade=' + idCidade,
                    data: 'data',
                    mimeType: 'json',
                    success: function (retorno) {
                        var option = new Array();
                        var dados = retorno.data;
                        resetaCombo('rodoviariaId');
                        $.each(dados, function (i, dados) {
                            var body = "<tr>";
                            option[i] = document.createElement('option');//criando o option
                            $(option[i]).attr({value: dados.idRodoviaria});//colocando o value no option
                            $(option[i]).append(dados.nomeRodoviaria);//colocando o 'label'
                            $("#rodoviariaId").append(option[i]);//jogando um à um os options no próximo combo
                        });
                    },
                    error: function () {
                        alert('Fail!');
                    }
                });
            }

            function filtraViacao() {
                var idRodoviaria = $('#rodoviariaId').val();
                $.ajax({
                    type: 'GET',
                    url: '<%=request.getContextPath()%>/getDados?cmd=listaviacoes&idRodoviaria=' + idRodoviaria,
                    data: 'data',
                    mimeType: 'json',
                    success: function (retorno) {
                        var option = new Array();
                        var dados = retorno.data;
                        resetaCombo('viacaoId');
                        $.each(dados, function (i, dados) {
                            var body = "<tr>";
                            option[i] = document.createElement('option');//criando o option
                            $(option[i]).attr({value: dados.idViacao});//colocando o value no option
                            $(option[i]).append(dados.nomeViacao);//colocando o 'label'
                            $("#viacaoId").append(option[i]);//jogando um à um os options no próximo combo
                        });
                    },
                    error: function () {
                        alert('Fail!');
                    }
                });
            }
        </script>
    </body>
</html>