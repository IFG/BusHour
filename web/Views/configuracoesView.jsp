<%-- 
    Document   : configuracaoView
    Created on : 09/11/2017, 08:19:56
    Author     : Cayo Eduardo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>BusHour</title>

        <!-- Bootstrap core CSS -->
        <link href="<%=request.getContextPath()%>/CSS/bootstrap.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/CSS/font-awesome.css" rel="stylesheet">

        <!-- Custom fonts for this template -->
        <!--<link href="<%=request.getContextPath()%>/Bootstrap/vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">-->
        <!--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">-->
        <!--<link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>-->
        <!--<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>-->
        <!--<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>-->


        <!-- Custom styles for this template -->
        <link href="<%=request.getContextPath()%>/CSS/estilo.css" rel="stylesheet">

        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
        <!-- Bootstrap core JavaScript -->                
        <script src="<%=request.getContextPath()%>/JS/jquery.js"></script>
        <script src="<%=request.getContextPath()%>/JS/bootstrap.bundle.js"></script>

        <!-- Custom scripts for this template -->
        <!--<script src="<%=request.getContextPath()%>/JS/javascript.js"></script>-->
        <script src="<%=request.getContextPath()%>/Scripts/jquery.validate.min.js"></script>
    </head>

    <body id="page-top">

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container-fluid">
                <a class="navbar-brand js-scroll-trigger" href="inicioView.jsp"><i class="fa fa-cog"></i> BusHour</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fa fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ml-auto">
                        <li class="nav-item fa">
                            <a class="nav-link js-scroll-trigger" href="horarioFiltroView.jsp"><i class="fa fa-clock-o"></i> Horários</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="configuracoesView.jsp"><i class="fa fa-cog"></i> Configurações</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="sobreNosView.jsp"><i class="fa fa-user-o"></i> Sobre Nós</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Header -->
        <header class="masthead" style="text-align: left">
            <form action="" id="formCadastro" method="post">
                <div class="container">
                    <div class="intro-text">
                        <center><b><h3>Configurações</h3></b></center>

                        <div style="color: red;text-align: center">
                            <h6>Escolha as Configurações Padrões de Entrada do BusHour.</h6>
                        </div>

                        <div class="col-sm-12 al">
                            <b><label>Estado Padrão</label></b>
                            <select onchange="filtraCidades()" id="estadoOrigemId" name="estadoOrigemId" class="form-control fa" style="background-color: #212529;color: #fed136">
                                <option value="-1">Escolha uma Opção</option>
                            </select>
                        </div>

                        <div class="col-sm-12 al">
                            <b><label>Cidade Padrão</label></b>
                            <select onchange="filtraRodoviarias()" id="cidadeOrigemId" name="cidadeOrigemId" class="form-control fa" style="background-color: #212529;color: #fed136">
                                <option value="-1">Escolha uma Opção</option>
                            </select>
                        </div>

                        <div class="col-sm-12 al">
                            <b><label>Rodoviária Padrão</label></b>
                            <select onchange="filtraViacao()" id="rodoviariaOrigemId" name="rodoviariaOrigemId" class="form-control fa" style="background-color: #212529;color: #fed136">
                                <option value="-1">Escolha uma Opção</option>
                            </select>
                        </div>

                        <div class="col-sm-12" style="text-align: center;margin-top: 10px">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Salvar Configurações</button>
                        </div>
                    </div>
                </div>
            </form>
        </header>   
        <script>
            $.validator.setDefaults({
                submitHandler: function () {
                    $("#formCadastro").attr('action', '../cadastrarConfiguracoes');
                    $("#formCadastro").submit();
                }
            });
            // validate the comment form when it is submitted
            function filtraCidades() {
                var idEstado = $('#estadoOrigemId').val();
                $.ajax({
                    type: 'GET',
                    url: '../getDados?cmd=listacidades&idEstado=' + idEstado,
                    data: 'data',
                    mimeType: 'json',
                    success: function (retorno) {
                        var option = new Array();
                        var dados = retorno.data;
                        resetaCombo('cidadeOrigemId');
                        $.each(dados, function (i, dados) {

                            var body = "<tr>";
                            option[i] = document.createElement('option');//criando o option
                            $(option[i]).attr({value: dados.idCidade});//colocando o value no option
                            $(option[i]).append(dados.nomeCidade);//colocando o 'label'

                            $("#cidadeOrigemId").append(option[i]);//jogando um à um os options no próximo combo
                        });
                    },
                    error: function () {
                        alert('Fail!');
                    }
                });

            }
            function filtraRodoviarias() {
                var idCidade = $('#cidadeOrigemId').val();

                $.ajax({
                    type: 'GET',
                    url: '../getDados?cmd=listarodoviarias&idCidade=' + idCidade,
                    data: 'data',
                    mimeType: 'json',
                    success: function (retorno) {
                        var option = new Array();
                        var dados = retorno.data;
                        resetaCombo('rodoviariaOrigemId');
                        $.each(dados, function (i, dados) {

                            var body = "<tr>";
                            option[i] = document.createElement('option');//criando o option
                            $(option[i]).attr({value: dados.idRodoviaria});//colocando o value no option
                            $(option[i]).append(dados.nomeRodoviaria);//colocando o 'label'

                            $("#rodoviariaOrigemId").append(option[i]);//jogando um à um os options no próximo combo
                        });
                    },
                    error: function () {
                        alert('Fail!');
                    }
                });
            }


            /* função pronta para ser reaproveitada, caso queira adicionar mais combos dependentes */
            function resetaCombo(el) {
                $("select[name='" + el + "']").empty();//retira os elementos antigos
                var option = document.createElement('option');
                $(option).attr({value: '0'});
                $(option).append('Escolha uma Opção');
                $("select[name='" + el + "']").append(option);
            }

            $(document).ready(function () {
                $.ajax({
                    type: 'GET',
                    url: '<%=request.getContextPath()%>/getDados?cmd=listaestados',
                    data: 'data',
                    mimeType: 'json',
                    success: function (retorno) {
                        var option = new Array();
                        var dados = retorno.data;
                        $.each(dados, function (i, dados) {
                            var body = "<tr>";
                            option[i] = document.createElement('option');//criando o option
                            $(option[i]).attr({value: dados.idEstado});//colocando o value no option
                            $(option[i]).append(dados.nomeEstado);//colocando o 'label'

                            $("#estadoOrigemId").append(option[i]);//jogando um à um os options no próximo combo
                        });
                    },
                    error: function () {
                        alert('Fail!');
                    }
                });

            });
        </script>
    </body>
</html>


