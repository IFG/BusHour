<%-- 
    Document   : horarioView
    Created on : 09/11/2017, 08:19:56
    Author     : Cayo Eduardo
--%>

<%@page import="Models.HorarioModel"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>BusHour</title>
        <!-- Bootstrap core CSS -->
        <link href="<%=request.getContextPath()%>/CSS/bootstrap.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/CSS/font-awesome.css" rel="stylesheet">

        <!-- Custom fonts for this template -->
        <!--<link href="<%=request.getContextPath()%>/Bootstrap/vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">-->
        <!--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">-->
        <!--<link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>-->
        <!--<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>-->
        <!--<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>-->

        <!-- Custom styles for this template -->
        <link href="<%=request.getContextPath()%>/CSS/estilo.css" rel="stylesheet">    </head>

    <body id="page-top">

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container-fluid">
                <a class="navbar-brand js-scroll-trigger" href="inicioView.jsp"><i class="fa fa-clock-o"></i> BusHour</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fa fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ml-auto">
                        <li class="nav-item fa">
                            <a class="nav-link js-scroll-trigger" href="horarioFiltroView.jsp"><i class="fa fa-clock-o"></i> Horários</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="configuracoesView.jsp"><i class="fa fa-cog"></i> Configurações</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="sobreNosView.jsp"><i class="fa fa-user-o"></i> Sobre Nós</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Header -->
        <header class="masthead" style="text-align: left">
            <div class="container">
                <div class="intro-text">
                    <center><b><h3>Horário(s)</h3></b></center>
                                <%
                                    ArrayList<HorarioModel> lista = (ArrayList<HorarioModel>) request.getAttribute("lista-dados-tabela");
                                    out.println("<table class='tabelaHorarios'>");
                                    out.println("<tr>");
                                    out.println("<td><b>Horário Saída</td>");
                                    out.println("<td><b>Horário Chegada</td>");
                                    for (HorarioModel hm : lista) {
                                        //out.println("<div>");
                                        out.println("<tr>");
                                        out.println("<td>" + hm.getHorarioSaida() + "</td>");
                                        out.println("<td>" + hm.getHorarioChegada() + "</td>");
                                        out.println("</tr>");
                                        //out.println("</div>");
                                    }
                                    out.println("</tr>");
                                    out.println("</table>");
                                %>
                    <div class="col-sm-12 al" style="text-align: left;margin-top: 10px">
                        <a class="nav-link js-scroll-trigger" href="./Views/horarioFiltroView.jsp"><button type="button" class="btn btn-primary"><i class="fa fa-reply-all"></i> Voltar</button></a>
                    </div>

                    <div>
                        <!--                        <script>
                                                    $(document).ready(function () {
                                                        $.ajax({
                                                            type: 'GET',
                                                            url: '../getDados?cmd=listahorario&idCidadeOrigem=' + idCidadeOrigem + '&idRodoviaria=' + idRodoviaria + '&idViacao=' + idViacao,
                                                            data: 'data',
                                                            mimeType: 'json',
                                                            success: function (retorno) {
                                                                var dados = retorno.data;
                                                                $.each(dados, function (i, dados) {
                                                                    var body = "<tr>";
                                                                    body += "<td>" + dados.idViacao + "</td>";
                                                                    body += "<td>" + dados.nomeViacao + "</td>";
                                                                    body += "<td>" + dados.telefoneViacao + "</td>";
                                                                    body += "</tr>";
                                                                    $("#tbody").append(body);
                                                                });
                                                                montaTabela();
                                                                /*DataTables instantiation.*/
                                                                $("#tabela-horario").DataTable();
                                                            },
                                                            error: function () {
                                                                alert('Fail!');
                                                            }
                                                        });
                                                    });
                        
                                                    function montaTabela() {
                                                        $('#tabela-horario').Tabledit({
                                                            url: '../../dados',
                                                            editButton: true,
                                                            columns: {
                                                                identifier: [0, 'idHorario'],
                                                                editable: [[1, 'horarioSaida'], [2, 'horarioChegada']]
                                                            },
                                                            onDraw: function () {
                                                                console.log('onDraw()');
                                                            },
                                                            onSuccess: function (data, textStatus, jqXHR) {
                                                                console.log('onSuccess(data, textStatus, jqXHR)');
                                                                console.log(data);
                                                                console.log(textStatus);
                                                                console.log(jqXHR);
                                                            },
                                                            onFail: function (jqXHR, textStatus, errorThrown) {
                                                                console.log('onFail(jqXHR, textStatus, errorThrown)');
                                                                console.log(jqXHR);
                                                                console.log(textStatus);
                                                                console.log(errorThrown);
                                                            },
                                                            onAlways: function () {
                                                                console.log('onAlways()');
                                                            },
                                                            onAjax: function (action, serialize) {
                                                                console.log('onAjax(action, serialize)');
                                                                console.log(action);
                                                                console.log(serialize);
                                                            }
                                                        });
                                                    }
                                                </script> -->
                    </div>
                </div>
            </div>  
        </header>   

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Bootstrap core JavaScript -->                
        <script src="../JS/jquery.js"></script>
        <script src="../JS/bootstrap.bundle.js"></script>
        <!-- Custom scripts for this template -->
        <script src="../JS/javascript.js"></script>
    </body>
</html>


