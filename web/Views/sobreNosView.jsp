<%-- 
    Document   : SobreNosView
    Created on : 09/11/2017, 08:20:24
    Author     : Cayo Eduardo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>BusHour</title>
        <!-- Bootstrap core CSS -->
        <link href="<%=request.getContextPath()%>/CSS/bootstrap.css" rel="stylesheet">
        <link href="<%=request.getContextPath()%>/CSS/font-awesome.css" rel="stylesheet">

        <!-- Custom fonts for this template -->
        <!--<link href="<%=request.getContextPath()%>/Bootstrap/vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">-->
        <!--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">-->
        <!--<link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>-->
        <!--<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>-->
        <!--<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>-->

        <!-- Custom styles for this template -->
        <link href="<%=request.getContextPath()%>/CSS/estilo.css" rel="stylesheet">    </head>

    <body id="page-top">

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container-fluid">
                <a class="navbar-brand js-scroll-trigger" href="inicioView.jsp"><i class="fa fa-user-o"></i> BusHour</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fa fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ml-auto">
                        <li class="nav-item fa">
                            <a class="nav-link js-scroll-trigger" href="horarioFiltroView.jsp"><i class="fa fa-clock-o"></i> Horários</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="configuracoesView.jsp"><i class="fa fa-cog"></i> Configurações</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="sobreNosView.jsp"><i class="fa fa-user-o"></i> Sobre Nós</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Header -->
        <header class="masthead" style="text-align: left">
            <div class="container">
                <div class="intro-text">
                    <center><b><h3>Sobre Nós</h3></b></center>
                </div>
            </div>  
        </header>   

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <!-- Bootstrap core JavaScript -->                
        <script src="../JS/jquery.js"></script>
        <script src="../JS/bootstrap.bundle.js"></script>

        <!-- Custom scripts for this template -->
        <script src="../JS/javascript.js"></script>

    </body>
</html>



